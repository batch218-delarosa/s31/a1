const http = require("http");

const port = 3000;

const server = http.createServer((req, res) => {

	const path = req.url;

	if (path == '/login') {
		res.writeHead(200, {"Content-Type": "text/text"});
		res.end('Welcome to the login page.')
	}  else {
		res.writeHead(404, {"Content-Type": "text/text"});
		res.end("I'm sorry the page you are looking for cannot be found."); 
	}

});

server.listen(port);

console.log(`Listening at port ${port}`);